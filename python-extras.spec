Name:           python-extras
Version:        1.0.0
Release:        9
Summary:        A set of extensions to the python standard library
License:        MIT
URL:            https://github.com/testing-cabal/extras
Source0:        https://pypi.io/packages/source/e/extras/extras-%{version}.tar.gz
Patch6000:      0001-Update-classifiers.patch
BuildArch:      noarch

%description
python-extras is a set of extensions to the standard library.

%package -n python3-extras
Summary:        A set of extensions to the python3 standard library
BuildRequires:  python3-devel python3-setuptools python3-testtools
%{?python_provide:%python_provide python3-extras}

%description -n python3-extras
python3-extras is a set of extensions to the standard library.

%prep
%autosetup -n extras-%{version}
rm -vrf *.egg-info

%build
%py3_build

%install
%py3_install

%check
%{__python3} -m testtools.run extras.tests.test_suite

%files -n python3-extras
%doc LICENSE NEWS README.rst
%{python3_sitelib}/extras/
%{python3_sitelib}/extras-*.egg-info/

%changelog
* Tue Jan 21 2025 yaoxin <1024769339@qq.com> - 1.0.0-9
- Modify test method for fix test error

* Mon Jul 17 2023 caofei <caofei@xfusion.com> -   1.0.0-8
- Update classifiers

* Mon Aug 10 2020 lingsheng <lingsheng@huawei.com> - 1.0.0-7
- Remove python2-extras subpackage

* Mon Nov 25 2019 Ling Yang <lingyang2@huawei.com> - 1.0.0-6
- Package init
